function $ (selector) { // eslint-disable-line no-unused-vars
    var results = [], classes = getDocumentClasses(), str, i, j, a;

    if (document.getElementsByTagName(selector)) {
        var t = document.getElementsByTagName(selector), 
            hasClass = selector.includes(".") ? true : false, 
            hasID = selector.includes("#") ? true : false,
            i;

        // console.log("selector: " + selector + " |", "class: " + hasClass + " |", "id: " + hasID);



        //**********************************************************/
        //** Search by both class and ID ***************************/
        if (hasClass && hasID) {
            // console.log("   document classes: " + classes);

            for (i = 0; i < classes.length; i++) {
                if (selector.includes(classes[i])) {
                    str = classes[i];
                    a = document.getElementsByClassName(str);

                    if (selector.includes(a[i].id)) results.push(a[i]);
                }
            }


        //**********************************************************/
        //** Search by class ***************************************/
        } else if (hasClass) {
            // console.log("   document classes: " + classes);

            for (i = 0; i < classes.length; i++) {
                if (selector.includes(classes[i])) {
                    str = classes[i];
                    a = document.getElementsByClassName(str);

                    for (j = 0; j < a.length; j++) {
                        if (selector.charAt(0) === ".") {
                            results.push(a[j]);
                        } else if (selector.includes(a[j].tagName.toLowerCase())) {
                            results.push(a[j]);
                        }
                    }
                }
            }


        //**********************************************************/
        //** Search by ID ******************************************/
        } else if (hasID) {
            if (selector.charAt(0) === "#") results.push(window[selector.substring(selector.indexOf("#") + 1, selector.length)]);


        //**********************************************************/
        //** Search by everything - in this case, by tag ***********/
        } else {
            for (var i = 0; i < t.length; i++) results.push(t[i]);
        }
    }

    return results;
}

function getDocumentClasses() {
    var elements = document.body.childNodes, i, j, results = [];

    for (i = 0; i < elements.length; i++) {
        var el = elements[i].nodeName, elID, elC, allClasses;

        // Filter # to remove text and comments etc
        if (!el.includes("#")) {    
            elID = elements[i].id;
            elC = elements[i].className.split(' ');
            // console.log(i, el, elID, elC);

            for (j = 0; j < elements[i].classList.length; j++) results.push(elements[i].classList[j]);
        }
    }

    results = unique(results);

    return results;
}

function unique($a) { return Array.from(new Set($a)); }